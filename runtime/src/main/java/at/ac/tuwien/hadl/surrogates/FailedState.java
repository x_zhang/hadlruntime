package at.ac.tuwien.hadl.surrogates;

import java.util.concurrent.ExecutorService;

import rx.subjects.PublishSubject;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;

class FailedState extends SurrogateState {

    FailedState(Surrogate surrogate,
            PublishSubject<SurrogateEvent> eventEmitter,
            ExecutorService executor, SurrogateStatus status) {
        super(surrogate, executor, status);
    }
}
