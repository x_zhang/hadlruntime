package at.ac.tuwien.hadl.domain.exception;

public class HADLException extends RuntimeException {
    private static final long serialVersionUID = -3256790743121229645L;

    public HADLException(String msg) {
        super(msg);
    }

    public HADLException(Exception e) {
        super(e);
    }
}
