package at.ac.tuwien.hadl.surrogates;

import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.exception.SurrogateException;

public class StateException extends SurrogateException {

    private static final long serialVersionUID = 7512005996754843934L;
    private SurrogateRequest request;

    public StateException(Surrogate surr, SurrogateRequest request,
            Throwable cause) {
        super(surr, "Request cannot be handled in current state: "
                + request.getClass().getCanonicalName(), cause);
        this.request = request;
    }

    public SurrogateRequest getRequest() {
        return request;
    }
}
