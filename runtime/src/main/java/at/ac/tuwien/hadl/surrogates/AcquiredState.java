package at.ac.tuwien.hadl.surrogates;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;

import rx.Observable;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;

class AcquiredState extends SurrogateState {

    private Semaphore eventSem = new Semaphore(0);

    public AcquiredState(Surrogate surrogate,
            ExecutorService executor) {
        super(surrogate, executor,
                SurrogateStatus.ACQUIRING_SUCCESS);
    }

    @Override
    public SurrogateState handle(SurrogateRequest request) {
        super.handle(request);
        if (request instanceof RequestConnect
                || request instanceof RequestRelease) {
            Observable<SurrogateEvent> obs = request.executeFor(surrogate);
            obs.subscribe(this::handleEvent);
            try {
                eventSem.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return nextState.get();
        } else {
            request.emit(new SurrogateEvent(
                    surrogate,
                    request.getFailedStatus(),
                    new StateException(surrogate, request, null)));
            return this;
        }
    }

    private void handleEvent(SurrogateEvent event) {
        switch (event.getStatus()) {
        case WIRING_SUCCESS:
            nextState.set(new WiredState(surrogate, executor));
            break;
        case WIRING_FAILED:
            nextState.set(this);
            break;
        case RELEASING_SUCCESS:
            nextState.set(new InitialState(surrogate, executor));
            break;
        case RELEASING_FAILED:
            nextState.set(this);
            break;
        default:
            break;
        }
        eventSem.release();
        SurrogateRequest request = currentRequest.get();
        if (request != null) {
            request.emit(event);
        }
    }
}
