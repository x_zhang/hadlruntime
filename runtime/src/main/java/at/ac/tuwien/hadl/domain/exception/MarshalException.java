package at.ac.tuwien.hadl.domain.exception;

public class MarshalException extends RuntimeException {
    private static final long serialVersionUID = 850968701504064315L;

    public MarshalException(Exception e) {
        super(e);
    }

    public MarshalException(String msg) {
        super(msg);
    }
}
