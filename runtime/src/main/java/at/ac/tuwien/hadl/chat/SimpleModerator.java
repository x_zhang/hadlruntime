package at.ac.tuwien.hadl.chat;

class SimpleModerator {

    private String name;

    public SimpleModerator(String name) {
        this.name = name;
        System.out.println(String.format("starting Party %s", name));
    }

    public String getName() {
        return name;
    }
}
