package at.ac.tuwien.hadl.registry;

import org.apache.camel.guice.CamelModuleWithMatchingRoutes;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class RegistryModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new CamelModuleWithMatchingRoutes());

        bind(HADLRegistry.class).to(CamelRegistry.class)
                .in(Singleton.class);
    }
}
