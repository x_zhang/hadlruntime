package at.ac.tuwien.hadl.chat;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.google.common.base.Joiner;

class SimpleChatroom {

    private List<SimpleModerator> mods = new CopyOnWriteArrayList<>();
    private List<SimpleParticipant> parts = new CopyOnWriteArrayList<>();

    private List<String> names = new CopyOnWriteArrayList<>();

    public SimpleChatroom(String uri) {
        System.out.println(String.format("...starting Chat: %s", uri));
    }

    public void addModerator(SimpleModerator moderator) {
        mods.add(moderator);
        names.add(moderator.getName());

        System.out.println("member joined");
        System.out.println(String.format("[%s]", Joiner.on(",").join(names)));
    }

    public void addParticipant(SimpleParticipant participant) {
        parts.add(participant);
        names.add(participant.getName());

        System.out.println("member joined");
        System.out.println(String.format("[%s]", Joiner.on(",").join(names)));
    }

    public void removeParticipant(SimpleParticipant participant) {
        parts.remove(participant);
        names.remove(participant.getName());

        System.out.println("member left");
        System.out.println(String.format("[%s]", Joiner.on(",").join(names)));
    }

    public void removeModerator(SimpleModerator moderator) {
        mods.remove(moderator);
        names.remove(moderator.getName());

        System.out.println("member left");
        System.out.println(String.format("[%s]", Joiner.on(",").join(names)));
    }
}
