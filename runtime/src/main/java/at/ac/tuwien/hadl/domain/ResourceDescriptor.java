package at.ac.tuwien.hadl.domain;

public interface ResourceDescriptor {

    String getResourceURI();

    Object getValue(String key);
}
