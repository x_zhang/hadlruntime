package at.ac.tuwien.hadl.surrogates;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import at.ac.tuwien.hadl.domain.ResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;
import rx.Observable;
import rx.subjects.ReplaySubject;

public class OperationManager {

    private Surrogate surrogate;
    private SurrogateDescriptor descriptor;
    private SurrogateState state;
    private BlockingQueue<SurrogateRequest> requests;
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public OperationManager(Surrogate surr, SurrogateDescriptor descriptor) {
        this.surrogate = surr;
        this.descriptor = descriptor;
        requests = new LinkedBlockingQueue<>();
        executor.execute(() -> processRequests());
        state = new InitialState(surr, executor);
    }

    private void processRequests() {
        while (true) {
            try {
                SurrogateRequest request = requests.take();
                state = state.handle(request);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Observable<SurrogateEvent> acquire(Scope forScope,
            ResourceDescriptor surrogateFor) {
        ReplaySubject<SurrogateEvent> emitter = ReplaySubject.create();
        requests.add(new RequestAcquire(forScope, surrogateFor, emitter));
        return emitter;
    }

    public Observable<SurrogateEvent> connectTo(Scope inScope,
            List<OperationManager> endpoints) {
        ReplaySubject<SurrogateEvent> emitter = ReplaySubject.create();
        requests.add(new RequestConnect(inScope, endpoints, emitter));
        return emitter;
    }

    public Observable<SurrogateEvent> release(Scope inScope) {
        ReplaySubject<SurrogateEvent> emitter = ReplaySubject.create();
        requests.add(new RequestRelease(inScope, emitter));
        return emitter;
    }

    public Observable<SurrogateEvent> disconnectFrom(Scope inScope,
            List<OperationManager> endpoints) {
        ReplaySubject<SurrogateEvent> emitter = ReplaySubject.create();
        requests.add(new RequestDisconnect(inScope, endpoints, emitter));
        return emitter;
    }

    public ResourceDescriptor getResourceDescriptor() {
        return surrogate.getResourceDescriptor();
    }

    public SurrogateDescriptor getDescriptor() {
        return descriptor;
    }

    public Surrogate getSurrogate() {
        return surrogate;
    }

    public void setSurrogate(Surrogate surrogate) {
        this.surrogate = surrogate;
    }

    public SurrogateStatus getStatus() {
        return this.state.getStatus();
    }

    public void shutdown() {
        executor.shutdownNow();
    }
}
