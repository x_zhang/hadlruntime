package at.ac.tuwien.hadl.marshalling;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class MarshallingModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(Names.named("marshalling_namespace"))
                .to("at.ac.tuwien.hadl.schema");
        bind(Marshaller.class).to(XmlMarshaller.class);
    }
}
