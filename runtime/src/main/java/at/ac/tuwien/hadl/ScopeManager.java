package at.ac.tuwien.hadl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import rx.Observable;
import at.ac.tuwien.hadl.domain.ResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.surrogates.OperationManager;
import at.ac.tuwien.hadl.surrogates.SurrogateDescriptor;

class ScopeManager {

    private Scope scope;
    private SurrogateFactory factory;

    private ModelDescriptor modelDescriptor;

    /**
     * Map of URI to descriptor
     */
    private Map<String, OperationManager> surrogateDescriptors = new ConcurrentHashMap<>();

    /**
     * Map of elementId to surrogates
     */
    private Map<String, List<OperationManager>> surrogateInstances = new ConcurrentHashMap<>();

    public ScopeManager(Scope scope, SurrogateFactory factory) {
        this.factory = factory;
        this.scope = scope;
        this.modelDescriptor = new ModelDescriptor(scope);
    }

    public Observable<SurrogateEvent> acquire(String elementId,
            final ResourceDescriptor rd) {

        String execVia = modelDescriptor
                .getExecutableFor(elementId);
        Surrogate surr = factory.create(execVia);
        SurrogateDescriptor des = new SurrogateDescriptor(
                elementId);

        if (!surrogateInstances.containsKey(elementId)) {
            surrogateInstances.put(elementId,
                    new ArrayList<OperationManager>());
        }

        OperationManager man = new OperationManager(surr,
                des);
        surrogateDescriptors.put(rd.getResourceURI(), man);
        surrogateInstances.get(elementId).add(man);

        return man.acquire(scope, rd);
    }

    public Observable<SurrogateEvent> wire(Surrogate surr) {
        OperationManager man = surrogateDescriptors.get(surr
                .getResourceDescriptor().getResourceURI());
        List<OperationManager> endpoints = modelDescriptor
                .getLinkEndpoints(man.getDescriptor().getElementId())
                .parallelStream()
                .flatMap(
                        endpointId -> surrogateInstances.get(endpointId)
                                .stream())
                .collect(Collectors.toList());
        return man.connectTo(scope, endpoints);
    }

    public Observable<SurrogateEvent> unwire(Surrogate surr) {
        OperationManager man = surrogateDescriptors.get(surr
                .getResourceDescriptor().getResourceURI());
        List<OperationManager> endpoints = modelDescriptor
                .getLinkEndpoints(man.getDescriptor().getElementId())
                .parallelStream()
                .flatMap(
                        endpointId -> surrogateInstances.get(endpointId)
                                .stream())
                .collect(Collectors.toList());
        return man.disconnectFrom(scope, endpoints);
    }

    public Observable<SurrogateEvent> release(Surrogate surr) {
        OperationManager man = surrogateDescriptors.get(surr
                .getResourceDescriptor().getResourceURI());
        return man.release(scope);
    }
}
