package at.ac.tuwien.hadl.surrogates;

import rx.Observable;
import rx.subjects.Subject;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;

class RequestRelease implements SurrogateRequest {

    private Scope scope;
    private Subject<SurrogateEvent, SurrogateEvent> emitter;

    public RequestRelease(Scope inScope,
            Subject<SurrogateEvent, SurrogateEvent> emitter) {
        this.scope = inScope;
        this.emitter = emitter;
    }

    @Override
    public Observable<SurrogateEvent> executeFor(Surrogate surrogate) {
        return surrogate.release(scope);
    }

    @Override
    public void emit(SurrogateEvent event) {
        emitter.onNext(event);
    }

    @Override
    public SurrogateStatus getSuccessStatus() {
        return SurrogateStatus.RELEASING_SUCCESS;
    }

    @Override
    public SurrogateStatus getFailedStatus() {
        return SurrogateStatus.RELEASING_FAILED;
    }
}
