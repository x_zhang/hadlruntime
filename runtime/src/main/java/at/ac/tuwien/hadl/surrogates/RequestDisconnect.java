package at.ac.tuwien.hadl.surrogates;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import rx.Observable;
import rx.subjects.ReplaySubject;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;
import at.ac.tuwien.hadl.domain.exception.MultiSurrogateException;

public class RequestDisconnect implements SurrogateRequest {

    private Scope scope;
    private List<OperationManager> endpoints;
    private ReplaySubject<SurrogateEvent> emitter;

    public RequestDisconnect(Scope inScope, List<OperationManager> endpoints,
            ReplaySubject<SurrogateEvent> emitter) {
        this.scope = inScope;
        this.endpoints = endpoints;
        this.emitter = emitter;
    }

    @Override
    public Observable<SurrogateEvent> executeFor(Surrogate surrogate) {
        final CountDownLatch latch = new CountDownLatch(endpoints.size());
        final Set<SurrogateEvent> failedConnections = Collections
                .synchronizedSet(new HashSet<>());
        for (OperationManager man : endpoints) {
            Observable<SurrogateEvent> obs = surrogate.disconnectFrom(scope,
                    man.getSurrogate());
            obs.subscribe(event -> {
                latch.countDown();
                if (!SurrogateStatus.UNWIRING_SUCCESS
                        .equals(event.getStatus())) {
                    failedConnections.add(event);
                }
            });
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (failedConnections.size() > 0) {
            Set<Throwable> causes = failedConnections.stream()
                    .map(event -> event.getOptionalEx())
                    .collect(Collectors.toSet());
            return Observable.just(new SurrogateEvent(surrogate,
                    SurrogateStatus.UNWIRING_FAILED,
                    new MultiSurrogateException(surrogate, "disconnect failed",
                            causes)));
        } else {
            return Observable.just(new SurrogateEvent(surrogate,
                    SurrogateStatus.UNWIRING_SUCCESS));
        }
    }

    @Override
    public void emit(SurrogateEvent event) {
        emitter.onNext(event);
    }

    @Override
    public SurrogateStatus getSuccessStatus() {
        return SurrogateStatus.UNWIRING_SUCCESS;
    }

    @Override
    public SurrogateStatus getFailedStatus() {
        return SurrogateStatus.UNWIRING_FAILED;
    }
}
