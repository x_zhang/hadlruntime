package at.ac.tuwien.hadl.core;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.schema.HADLmodel;
import at.ac.tuwien.hadl.schema.TCollabConnector;
import at.ac.tuwien.hadl.schema.TCollabLink;
import at.ac.tuwien.hadl.schema.TCollabObject;
import at.ac.tuwien.hadl.schema.TExecutables;
import at.ac.tuwien.hadl.schema.THADLarchElement;
import at.ac.tuwien.hadl.schema.THumanComponent;
import at.ac.tuwien.hadl.schema.TStructure;

public class SimpleScope implements Scope {

    private String id;

    private List<TCollabConnector> connectors = new ArrayList<>();
    private List<TCollabObject> objects = new ArrayList<>();
    private List<THumanComponent> components = new ArrayList<>();
    private List<TCollabLink> links = new ArrayList<>();
    private List<TExecutables> executables = new ArrayList<>();

    public SimpleScope() {

    }

    public SimpleScope(HADLmodel model) {
        for (TStructure s : model.getHADLstructure()) {
            connectors.addAll(s.getConnector());
            objects.addAll(s.getObject());
            components.addAll(s.getComponent());
            links.addAll(s.getLink());
        }

        for (THADLarchElement.Extension e : model.getExtension()) {
            for (Object o : e.getAny()) {
                if (o.getClass().isAssignableFrom(JAXBElement.class)) {
                    @SuppressWarnings("rawtypes")
                    JAXBElement wrapper = (JAXBElement) o;
                    if (wrapper.getValue().getClass()
                            .isAssignableFrom(TExecutables.class)) {
                        executables.add((TExecutables) wrapper.getValue());
                    }
                }
            }
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public List<TCollabConnector> getConnectors() {
        return connectors;
    }

    @Override
    public List<TCollabObject> getObjects() {
        return objects;
    }

    @Override
    public List<THumanComponent> getComponents() {
        return components;
    }

    @Override
    public List<TCollabLink> getLinks() {
        return links;
    }

    @Override
    public boolean existsElement(String hADLelementId) {
        List<THADLarchElement> structures = new ArrayList<>();
        structures.addAll(connectors);
        structures.addAll(objects);
        structures.addAll(components);

        for (THADLarchElement e : structures) {
            if (hADLelementId.equals(e.getName())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public List<TExecutables> getExecutables() {
        return executables;
    }
}
