package at.ac.tuwien.hadl.domain;

import rx.Observable;

public interface Surrogate {
    public Observable<SurrogateEvent> acquire(Scope forScope,
            ResourceDescriptor surrogateFor);

    public Observable<SurrogateEvent> release(Scope inScope);

    public Observable<SurrogateEvent> connectTo(Scope inScope,
            Surrogate endpoint);

    public Observable<SurrogateEvent> disconnectFrom(Scope inScope,
            Surrogate endpoint);

    public ResourceDescriptor getResourceDescriptor();
}