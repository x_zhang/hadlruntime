package at.ac.tuwien.hadl.domain.exception;

import java.util.Set;

import at.ac.tuwien.hadl.domain.Surrogate;

public class MultiSurrogateException extends SurrogateException {

    private static final long serialVersionUID = -665704846853920222L;
    private Set<Throwable> causes;

    public MultiSurrogateException(Surrogate surr, String msg,
            Throwable cause) {
        super(surr, msg, cause);
    }

    public MultiSurrogateException(Surrogate surr, String msg,
            Set<Throwable> causes) {
        super(surr, msg, null);
        this.causes = causes;
    }

    public Set<Throwable> getCauses() {
        return causes;
    }
}
