package at.ac.tuwien.hadl.core;

import java.util.HashMap;
import java.util.Map;

import at.ac.tuwien.hadl.domain.ResourceDescriptor;

public class SimpleResourceDescriptor implements ResourceDescriptor {

    private Map<String, Object> parameters = new HashMap<>();
    private String uri;

    public SimpleResourceDescriptor(String uri) {
        this.uri = uri;
    }

    @Override
    public String getResourceURI() {
        return uri;
    }

    @Override
    public Object getValue(String key) {
        return parameters.get(key);
    }

    public void addValue(String key, Object value) {
        parameters.put(key, value);
    }
}
