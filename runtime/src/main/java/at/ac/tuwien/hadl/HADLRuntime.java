package at.ac.tuwien.hadl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;

import at.ac.tuwien.hadl.core.SimpleScope;
import at.ac.tuwien.hadl.domain.ResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.exception.HADLException;
import at.ac.tuwien.hadl.marshalling.Marshaller;
import at.ac.tuwien.hadl.schema.HADLmodel;
import at.ac.tuwien.hadl.schema.TExecutables;
import rx.Observable;

public class HADLRuntime {
    private Injector injector;

    @Inject
    private Marshaller marshaller;

    @Inject
    private Provider<SurrogateFactory> factoryProvider;

    private Map<String, ScopeManager> scopeMap = new HashMap<>();

    public HADLRuntime() {
    }

    public Scope allocate(Scope scope)
            throws HADLException {
        SurrogateFactory factory = factoryProvider.get();
        factory.addExecutableMappings(scope.getExecutables().toArray(
                new TExecutables[0]));
        scope.setId(UUID.randomUUID().toString());
        ScopeManager scopeManager = new ScopeManager(scope, factory);
        scopeMap.put(scope.getId(), scopeManager);
        return scope;
    }

    public Observable<SurrogateEvent> registerFor(Scope scope,
            String elementId, ResourceDescriptor rd) {
        ScopeManager man = scopeMap.get(scope.getId());
        return man.acquire(elementId, rd);
    }

    public void init() {
        injector = Guice.createInjector(new HADLRuntimeModule());
        injector.injectMembers(this);
        marshaller.init();
    }

    public Scope createScopeFromResource(InputStream is) {
        HADLmodel model = marshaller.unmarshal(is);
        Scope scope = new SimpleScope(model);
        return scope;
    }

    public Observable<SurrogateEvent> wire(Surrogate surr, Scope scope) {
        return scopeMap.get(scope.getId()).wire(surr);
    }

    public Observable<SurrogateEvent> unwire(Surrogate surr, Scope scope) {
        return scopeMap.get(scope.getId()).unwire(surr);
    }

    public Observable<SurrogateEvent> release(Surrogate surr, Scope scope) {
        return scopeMap.get(scope.getId()).release(surr);
    }
}