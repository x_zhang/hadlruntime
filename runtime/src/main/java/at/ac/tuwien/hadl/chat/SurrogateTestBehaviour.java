package at.ac.tuwien.hadl.chat;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import at.ac.tuwien.hadl.domain.ResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;
import at.ac.tuwien.hadl.domain.exception.SurrogateException;
import at.ac.tuwien.hadl.surrogates.AsyncSurrogate;

public abstract class SurrogateTestBehaviour extends AsyncSurrogate {
    private final int maxTimeoutMS = 2000;
    private final int minTimeoutMS = 100;
    private Random random;

    private Map<SurrogateAction, Behaviour> behaviourMap = new ConcurrentHashMap<>();

    private ResourceDescriptor resourceDescriptor;

    public SurrogateTestBehaviour() {
        random = new Random(System.currentTimeMillis());
    }

    private void sleepTimeout() {
        try {
            Thread.sleep(random.nextInt(maxTimeoutMS - minTimeoutMS)
                    + minTimeoutMS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void extractBehaviour(ResourceDescriptor rd) {
        for (SurrogateAction a : SurrogateAction.values()) {
            Object o = rd.getValue(a.toString());
            if (o != null) {
                behaviourMap.put(a, (Behaviour) o);
            }
        }
    }

    @Override
    public SurrogateEvent acquireSurrogate(Scope forScope,
            ResourceDescriptor surrogateFor) {
        resourceDescriptor = surrogateFor;
        extractBehaviour(surrogateFor);
        sleepTimeout();
        Behaviour b = behaviourMap.get(SurrogateAction.ACQUIRE);
        if (b == Behaviour.FAIL) {
            return new SurrogateEvent(this, SurrogateStatus.ACQUIRING_FAILED,
                    new SurrogateException(
                            this, "Cannot acquire", null));
        } else if (b == Behaviour.TIMEOUT) {
            return new SurrogateEvent(this,
                    SurrogateStatus.ACQUIRING_FAILED, new SurrogateException(
                            this, "Timeout", null));
        }
        return real_acquire(forScope, surrogateFor);
    }

    protected abstract SurrogateEvent real_acquire(Scope s,
            ResourceDescriptor rd);

    @Override
    public SurrogateEvent releaseSurrogate(Scope inScope) {
        sleepTimeout();
        Behaviour b = behaviourMap.get(SurrogateAction.RELEASE);
        if (b == Behaviour.FAIL) {
            return new SurrogateEvent(this,
                    SurrogateStatus.RELEASING_FAILED, new SurrogateException(
                            this, "Cannot release", null));
        } else if (b == Behaviour.TIMEOUT) {
            return new SurrogateEvent(this,
                    SurrogateStatus.RELEASING_FAILED, new SurrogateException(
                            this, "Timeout", null));
        }
        return real_release(inScope);
    }

    protected abstract SurrogateEvent real_release(Scope scope);

    @Override
    public SurrogateEvent connectToSurrogate(Scope inScope,
            Surrogate endpoint) {
        sleepTimeout();
        Behaviour b = behaviourMap.get(SurrogateAction.WIRE);
        if (b == Behaviour.FAIL) {
            return new SurrogateEvent(this,
                    SurrogateStatus.WIRING_FAILED, new SurrogateException(
                            this, "Cannot wire", null));
        } else if (b == Behaviour.TIMEOUT) {
            return new SurrogateEvent(this,
                    SurrogateStatus.WIRING_FAILED, new SurrogateException(
                            this, "Timeout", null));
        }
        return real_connectTo(inScope, endpoint);
    }

    protected abstract SurrogateEvent real_connectTo(Scope scope,
            Surrogate endpoint);

    @Override
    public SurrogateEvent disconnectFromSurrogate(Scope inScope,
            Surrogate endpoint) {
        sleepTimeout();
        Behaviour b = behaviourMap.get(SurrogateAction.UNWIRE);
        if (b == Behaviour.FAIL) {
            return new SurrogateEvent(this,
                    SurrogateStatus.UNWIRING_FAILED, new SurrogateException(
                            this, "Cannot unwire", null));
        } else if (b == Behaviour.TIMEOUT) {
            return new SurrogateEvent(this,
                    SurrogateStatus.UNWIRING_FAILED, new SurrogateException(
                            this, "Timeout", null));
        }
        return real_disconnectFrom(inScope, endpoint);
    }

    protected abstract SurrogateEvent real_disconnectFrom(
            Scope scope, Surrogate endpoint);

    @Override
    public ResourceDescriptor getResourceDescriptor() {
        return resourceDescriptor;
    }
}
