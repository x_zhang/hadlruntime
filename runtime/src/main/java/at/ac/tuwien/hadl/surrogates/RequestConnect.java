package at.ac.tuwien.hadl.surrogates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;
import at.ac.tuwien.hadl.domain.exception.MultiSurrogateException;
import rx.Observable;
import rx.subjects.Subject;

public class RequestConnect implements SurrogateRequest {

    private List<OperationManager> endpoints;
    private Scope scope;
    private Subject<SurrogateEvent, SurrogateEvent> emitter;

    public RequestConnect(Scope inScope, List<OperationManager> endpoints,
            Subject<SurrogateEvent, SurrogateEvent> emitter) {
        this.scope = inScope;
        this.endpoints = endpoints;
        this.emitter = emitter;
    }

    @Override
    public Observable<SurrogateEvent> executeFor(Surrogate surrogate) {
        final Set<SurrogateEvent> failedConnections = Collections
                .synchronizedSet(new HashSet<>());
        List<Observable<SurrogateEvent>> observables = new ArrayList<>();
        for (final OperationManager man : endpoints) {
            Observable<SurrogateEvent> obs = surrogate.connectTo(
                    scope, man.getSurrogate());
            obs.subscribe(event -> {
                if (!SurrogateStatus.WIRING_SUCCESS.equals(event.getStatus())) {
                    failedConnections.add(event);
                }
            });
            observables.add(obs);
        }

        Observable.combineLatest(observables, os -> os);

        if (failedConnections.size() > 0) {
            Set<Throwable> causes = failedConnections.stream()
                    .map(event -> event.getOptionalEx())
                    .collect(Collectors.toSet());
            return Observable.just(new SurrogateEvent(surrogate,
                    SurrogateStatus.WIRING_FAILED,
                    new MultiSurrogateException(surrogate, "connection failed",
                            causes)));
        } else {
            return Observable.just(new SurrogateEvent(surrogate,
                    SurrogateStatus.WIRING_SUCCESS));
        }
    }

    @Override
    public void emit(SurrogateEvent event) {
        emitter.onNext(event);
    }

    @Override
    public SurrogateStatus getSuccessStatus() {
        return SurrogateStatus.WIRING_SUCCESS;
    }

    @Override
    public SurrogateStatus getFailedStatus() {
        return SurrogateStatus.WIRING_FAILED;
    }
}
