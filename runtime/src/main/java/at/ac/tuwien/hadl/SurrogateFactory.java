package at.ac.tuwien.hadl;

import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.schema.TExecutables;

public interface SurrogateFactory {

    void addExecutableMappings(TExecutables... mappings);

    Surrogate create(String elementId);
}
