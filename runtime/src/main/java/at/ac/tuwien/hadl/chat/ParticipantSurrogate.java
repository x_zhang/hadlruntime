package at.ac.tuwien.hadl.chat;

import at.ac.tuwien.hadl.domain.ResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;

public class ParticipantSurrogate extends SurrogateTestBehaviour {

    private SimpleParticipant participant;

    @Override
    public SurrogateEvent real_acquire(Scope forScope,
            ResourceDescriptor rd) {
        participant = new SimpleParticipant((String) rd.getValue("name"));
        return new SurrogateEvent(this,
                SurrogateStatus.ACQUIRING_SUCCESS);
    }

    @Override
    public SurrogateEvent real_release(Scope inScope) {
        return new SurrogateEvent(this,
                SurrogateStatus.RELEASING_SUCCESS);
    }

    @Override
    public SurrogateEvent real_connectTo(Scope inScope,
            Surrogate endpoint) {
        System.out.println("starting wiring " + participant.getName());
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (endpoint instanceof ChatroomSurrogate) {
            SimpleChatroom room = ((ChatroomSurrogate) endpoint).getChatroom();
            room.addParticipant(participant);
            return new SurrogateEvent(this,
                    SurrogateStatus.WIRING_SUCCESS);
        } else {
            return new SurrogateEvent(this,
                    SurrogateStatus.WIRING_FAILED);
        }

    }

    @Override
    public SurrogateEvent real_disconnectFrom(Scope inScope,
            Surrogate endpoint) {
        if (endpoint instanceof ChatroomSurrogate) {
            SimpleChatroom room = ((ChatroomSurrogate) endpoint).getChatroom();
            room.removeParticipant(participant);
            return new SurrogateEvent(this,
                    SurrogateStatus.UNWIRING_SUCCESS);
        } else {
            return new SurrogateEvent(this,
                    SurrogateStatus.UNWIRING_FAILED);
        }
    }
}
