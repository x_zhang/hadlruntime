package at.ac.tuwien.hadl.chat;

class SimpleParticipant {

    private String name;

    public SimpleParticipant(String name) {
        this.name = name;
        System.out.println(String.format("starting Party %s", name));
    }

    public String getName() {
        return name;
    }

}
