package at.ac.tuwien.hadl.registry;

import org.apache.camel.impl.SimpleRegistry;

import at.ac.tuwien.hadl.registry.HADLRegistry;

class CamelRegistry implements HADLRegistry {

    private SimpleRegistry registry;

    @Override
    public void start() {
    }

    @Override
    public void register(String uri, Object service) {
        registry.put(uri, service);
    }

    @Override
    public Object unregister(String uri) {
        return registry.remove(uri);
    }

    @Override
    public <T> T lookup(String resourceURI, Class<T> clazz) {
        return registry.lookup(resourceURI, clazz);
    }
}
