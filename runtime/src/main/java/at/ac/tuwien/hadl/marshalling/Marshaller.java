package at.ac.tuwien.hadl.marshalling;

import java.io.InputStream;

import at.ac.tuwien.hadl.schema.HADLmodel;

public interface Marshaller {

    HADLmodel unmarshal(String pathToFile);

    HADLmodel unmarshal(InputStream stream);

    void init();
}
