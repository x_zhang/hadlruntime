package at.ac.tuwien.hadl.surrogates;

import rx.Observable;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;

interface SurrogateRequest {

    Observable<SurrogateEvent> executeFor(Surrogate surrogate);

    void emit(SurrogateEvent event);

    SurrogateStatus getSuccessStatus();

    SurrogateStatus getFailedStatus();
}
