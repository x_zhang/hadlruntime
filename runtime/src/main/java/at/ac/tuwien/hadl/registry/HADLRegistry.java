package at.ac.tuwien.hadl.registry;

public interface HADLRegistry {
    void start();

    void register(String uri, Object service);

    Object unregister(String uri);

    <T> T lookup(String resourceURI, Class<T> clazz);
}
