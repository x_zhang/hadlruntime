package at.ac.tuwien.hadl.surrogates;

import at.ac.tuwien.hadl.domain.ResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import rx.Observable;
import rx.util.async.Async;

public abstract class AsyncSurrogate implements Surrogate {
    @Override
    public Observable<SurrogateEvent> acquire(Scope forScope,
            ResourceDescriptor surrogateFor) {
        return Async.start(() -> acquireSurrogate(forScope, surrogateFor));
    }

    @Override
    public Observable<SurrogateEvent> release(Scope inScope) {
        return Async.start(() -> releaseSurrogate(inScope));
    }

    @Override
    public Observable<SurrogateEvent> connectTo(Scope inScope,
            Surrogate endpoint) {
        return Async.start(() -> connectToSurrogate(inScope, endpoint));
    }

    @Override
    public Observable<SurrogateEvent> disconnectFrom(Scope inScope,
            Surrogate endpoint) {
        return Async.start(() -> disconnectFromSurrogate(inScope, endpoint));
    }

    public abstract SurrogateEvent acquireSurrogate(Scope forScope,
            ResourceDescriptor surrogateFor);

    public abstract SurrogateEvent releaseSurrogate(Scope inScope);

    public abstract SurrogateEvent connectToSurrogate(Scope inScope,
            Surrogate endpoint);

    public abstract SurrogateEvent disconnectFromSurrogate(Scope inScope,
            Surrogate endpoint);
}
