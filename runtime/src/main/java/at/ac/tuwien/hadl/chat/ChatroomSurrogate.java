package at.ac.tuwien.hadl.chat;

import at.ac.tuwien.hadl.domain.ResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;
import at.ac.tuwien.hadl.surrogates.AsyncSurrogate;

public class ChatroomSurrogate extends SurrogateTestBehaviour {

    private SimpleChatroom chatroom;

    @Override
    public SurrogateEvent real_acquire(Scope forScope,
            ResourceDescriptor rd) {
        chatroom = new SimpleChatroom((String) rd.getValue("uri"));
        return new SurrogateEvent(this,
                SurrogateStatus.ACQUIRING_SUCCESS);
    }

    @Override
    public SurrogateEvent real_release(Scope inScope) {
        return new SurrogateEvent(this,
                SurrogateStatus.RELEASING_SUCCESS);
    }

    @Override
    public SurrogateEvent real_connectTo(Scope inScope,
            Surrogate endpoint) {
        return ((AsyncSurrogate) endpoint).connectToSurrogate(inScope, this);
    }

    @Override
    public SurrogateEvent real_disconnectFrom(Scope inScope,
            Surrogate endpoint) {
        return ((AsyncSurrogate) endpoint).disconnectFromSurrogate(inScope,
                this);
    }

    public SimpleChatroom getChatroom() {
        return chatroom;
    }
}
