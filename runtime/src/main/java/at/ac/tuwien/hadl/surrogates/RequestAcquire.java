package at.ac.tuwien.hadl.surrogates;

import rx.Observable;
import rx.subjects.Subject;
import at.ac.tuwien.hadl.domain.ResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;

public class RequestAcquire implements SurrogateRequest {

    private Scope scope;
    private ResourceDescriptor resources;
    private Subject<SurrogateEvent, SurrogateEvent> emitter;

    public RequestAcquire(Scope forScope, ResourceDescriptor surrogateFor,
            Subject<SurrogateEvent, SurrogateEvent> emitter) {
        this.scope = forScope;
        this.resources = surrogateFor;
        this.emitter = emitter;
    }

    @Override
    public Observable<SurrogateEvent> executeFor(Surrogate surrogate) {
        return surrogate.acquire(scope, resources);
    }

    @Override
    public void emit(SurrogateEvent event) {
        emitter.onNext(event);
    }

    @Override
    public SurrogateStatus getSuccessStatus() {
        return SurrogateStatus.ACQUIRING_SUCCESS;
    }

    @Override
    public SurrogateStatus getFailedStatus() {
        return SurrogateStatus.ACQUIRING_FAILED;
    }
}
