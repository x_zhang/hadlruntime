package at.ac.tuwien.hadl.marshalling;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import at.ac.tuwien.hadl.domain.exception.MarshalException;
import at.ac.tuwien.hadl.schema.HADLmodel;

import com.google.inject.Inject;
import com.google.inject.name.Named;

class XmlMarshaller implements Marshaller {

    private JAXBContext context;
    private Unmarshaller unmarshaller;
    private String namespace;

    @Inject
    public XmlMarshaller(@Named("marshalling_namespace") String ns) {
        this.namespace = ns;
    }

    @Override
    public HADLmodel unmarshal(String pathToFile) {
        if (unmarshaller == null) {
            throw new MarshalException(
                    "Marshaller not initialized, call init() first.");
        }

        try {
            InputStream is = new FileInputStream(pathToFile);
            return unmarshal(is);
        } catch (FileNotFoundException e) {
            throw new MarshalException(e);
        }
    }

    @Override
    public HADLmodel unmarshal(InputStream stream) {
        if (unmarshaller == null) {
            throw new MarshalException(
                    "Marshaller not initialized, call init() first.");
        }

        try {
            StreamSource source = new StreamSource(stream);
            JAXBElement<HADLmodel> o = unmarshaller.unmarshal(source,
                    HADLmodel.class);
            return o.getValue();
        } catch (JAXBException | ClassCastException e) {
            throw new MarshalException(e);
        }
    }

    @Override
    public void init() {
        try {
            context = JAXBContext.newInstance(namespace);
            unmarshaller = context.createUnmarshaller();
        } catch (JAXBException e) {
            throw new MarshalException(e);
        }
    }
}