package at.ac.tuwien.hadl.surrogates;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;

import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateStatus;

abstract class SurrogateState {

    protected Surrogate surrogate;
    protected ExecutorService executor;
    protected SurrogateStatus status;
    protected AtomicReference<SurrogateState> nextState;
    protected AtomicReference<SurrogateRequest> currentRequest;

    SurrogateState(Surrogate surrogate,
            ExecutorService executor, SurrogateStatus status) {
        this.surrogate = surrogate;
        this.executor = executor;
        this.status = status;
        currentRequest = new AtomicReference<>();
        nextState = new AtomicReference<>();
    }

    SurrogateState handle(SurrogateRequest request) {
        this.currentRequest.set(request);
        return this;
    }

    SurrogateStatus getStatus() {
        return status;
    }
}
