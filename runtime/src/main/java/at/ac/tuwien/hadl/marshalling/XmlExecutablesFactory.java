package at.ac.tuwien.hadl.marshalling;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import at.ac.tuwien.hadl.schema.TExecutables;

@XmlRegistry
class XmlExecutablesFactory {
    @XmlElementDecl(name = "Executables")
    public JAXBElement<TExecutables> createBar(TExecutables tex) {
        return new JAXBElement<TExecutables>(new QName("Executables"),
                TExecutables.class, tex);
    }
}
