package at.ac.tuwien.hadl.chat;

public enum SurrogateAction {
    ACQUIRE, WIRE, UNWIRE, RELEASE
}
