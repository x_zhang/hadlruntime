package at.ac.tuwien.hadl.core;

import at.ac.tuwien.hadl.SurrogateFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class FactoryModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(SurrogateFactory.class).to(SimpleSurrogateFactory.class).in(
                Singleton.class);
    }
}