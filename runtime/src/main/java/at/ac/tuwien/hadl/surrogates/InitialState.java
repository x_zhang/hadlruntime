package at.ac.tuwien.hadl.surrogates;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Semaphore;

import rx.Observable;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;

class InitialState extends SurrogateState {

    private Semaphore eventSem = new Semaphore(0);

    public InitialState(Surrogate surrogate,
            ExecutorService executor) {
        super(surrogate, executor, SurrogateStatus.PREPARED);
    }

    @Override
    public SurrogateState handle(SurrogateRequest request) {
        super.handle(request);
        if (request instanceof RequestAcquire) {
            Observable<SurrogateEvent> obs = request.executeFor(surrogate);
            obs.subscribe(this::handleEvent);
            try {
                eventSem.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return nextState.get();
        } else {
            request.emit(new SurrogateEvent(
                    surrogate,
                    request.getFailedStatus(),
                    new StateException(surrogate, request, null)));
            return this;
        }
    }

    private void handleEvent(SurrogateEvent event) {
        switch (event.getStatus()) {
        case ACQUIRING_SUCCESS:
            nextState.set(new AcquiredState(surrogate, executor));
            break;
        case ACQUIRING_FAILED:
            nextState.set(this);
            break;
        default:
            break;
        }
        eventSem.release();
        SurrogateRequest request = currentRequest.get();
        if (request != null) {
            request.emit(event);
        }
    }
}
