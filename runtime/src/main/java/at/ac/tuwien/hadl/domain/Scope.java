package at.ac.tuwien.hadl.domain;

import java.util.List;

import at.ac.tuwien.hadl.schema.TCollabConnector;
import at.ac.tuwien.hadl.schema.TCollabLink;
import at.ac.tuwien.hadl.schema.TCollabObject;
import at.ac.tuwien.hadl.schema.TExecutables;
import at.ac.tuwien.hadl.schema.THumanComponent;

public interface Scope {
    public String getId();

    public void setId(String id);

    public List<TCollabConnector> getConnectors();

    public List<TCollabObject> getObjects();

    public List<THumanComponent> getComponents();

    public List<TExecutables> getExecutables();

    public boolean existsElement(String hADLelementId);

    List<TCollabLink> getLinks();
}
