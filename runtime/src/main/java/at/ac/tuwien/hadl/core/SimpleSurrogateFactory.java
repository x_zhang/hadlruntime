package at.ac.tuwien.hadl.core;

import java.util.HashMap;
import java.util.Map;

import at.ac.tuwien.hadl.SurrogateFactory;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.exception.HADLException;
import at.ac.tuwien.hadl.schema.TExecutables;
import at.ac.tuwien.hadl.schema.TSurrogate;

class SimpleSurrogateFactory implements SurrogateFactory {

    private Map<String, String> classMap = new HashMap<>();

    @Override
    public Surrogate create(String elementId) {
        Class<?> clazz;
        try {
            clazz = Class.forName(classMap.get(elementId));
            if (Surrogate.class.isAssignableFrom(clazz)) {
                Surrogate surr = (Surrogate) clazz.newInstance();
                return surr;
            }
        } catch (Exception e) {
            throw new HADLException(e);
        }

        throw new HADLException(String.format(
                "%s does not implement Surrogate Interface",
                clazz.getCanonicalName()));
    }

    @Override
    public void addExecutableMappings(TExecutables... mappings) {
        for (TExecutables e : mappings) {
            for (TSurrogate s : e.getSurrogate()) {
                classMap.put(s.getId(), s.getSurrogateFQN());
            }
        }
    }
}
