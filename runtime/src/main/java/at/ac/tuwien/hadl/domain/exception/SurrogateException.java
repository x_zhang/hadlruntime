package at.ac.tuwien.hadl.domain.exception;

import at.ac.tuwien.hadl.domain.Surrogate;

public class SurrogateException extends Exception {

    private static final long serialVersionUID = -5287156050210740034L;

    private Surrogate surr = null;

    public SurrogateException(Surrogate surr, String msg, Throwable cause) {
        super(msg, cause);
        this.surr = surr;
    }

    public Surrogate getSurr() {
        return surr;
    }
}
