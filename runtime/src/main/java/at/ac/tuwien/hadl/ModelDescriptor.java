package at.ac.tuwien.hadl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.schema.TAction;
import at.ac.tuwien.hadl.schema.TCollabLink;
import at.ac.tuwien.hadl.schema.TCollabObject;
import at.ac.tuwien.hadl.schema.TCollaborator;
import at.ac.tuwien.hadl.schema.TExecutableComponent;
import at.ac.tuwien.hadl.schema.TExecutableConnector;
import at.ac.tuwien.hadl.schema.TExecutableObject;
import at.ac.tuwien.hadl.schema.THADLarchElement;

import com.google.common.collect.Maps;

class ModelDescriptor {
    private Scope scope;

    /**
     * maps each element-id to the corresponding executable-id
     */
    private Map<String, String> executableViaSurrogateMap = Maps
            .newConcurrentMap();

    /**
     * Maps each action of a collaborator to the collaborator using the
     * action-id
     */
    private Map<String, TCollaborator> actionToCollaboratorMap = Maps
            .newConcurrentMap();

    /**
     * Maps each action of an object to the object using the action-id
     */
    private Map<String, TCollabObject> actionToObjectMap = Maps
            .newConcurrentMap();

    public ModelDescriptor(Scope scope) {
        this.scope = scope;
        loadInformation();
    }

    private void loadInformation() {
        scope.getComponents()
                .parallelStream()
                .filter(obj -> obj instanceof TExecutableComponent)
                .map(obj -> (TExecutableComponent) obj)
                .forEach(
                        obj -> mapExecutable(obj,
                                obj.getExecutableViaSurrogate(),
                                obj.getAction(),
                                actionToCollaboratorMap));
        scope.getObjects()
                .parallelStream()
                .filter(obj -> obj instanceof TExecutableObject)
                .map(obj -> (TExecutableObject) obj)
                .forEach(
                        obj -> mapExecutable(obj,
                                obj.getExecutableViaSurrogate(),
                                obj.getAction(),
                                actionToObjectMap));
        scope.getConnectors()
                .parallelStream()
                .filter(obj -> obj instanceof TExecutableConnector)
                .map(obj -> (TExecutableConnector) obj)
                .forEach(
                        obj -> mapExecutable(obj,
                                obj.getExecutableViaSurrogate(),
                                obj.getAction(),
                                actionToCollaboratorMap));
    }

    private void mapExecutable(THADLarchElement element, List<String> refs) {
        // TODO: select using collabPlatformRef
        // TODO: change string to surrogateRef
        final int COLLAB_PLATFORM = 0;
        if (refs.size() != 0) {
            executableViaSurrogateMap.put(element.getId(),
                    refs.get(COLLAB_PLATFORM));
        }
    }

    private void mapExecutable(TCollabObject element, List<String> refs,
            List<TAction> actions,
            Map<String, TCollabObject> actionToElementMap) {
        mapExecutable(element, refs);
        actions.parallelStream().forEach(
                action -> actionToElementMap.put(action.getId(), element));
    }

    private void mapExecutable(TCollaborator element, List<String> refs,
            List<TAction> actions,
            Map<String, TCollaborator> actionToElementMap) {
        mapExecutable(element, refs);
        actions.parallelStream().forEach(
                action -> actionToElementMap.put(action.getId(), element));
    }

    public String getExecutableFor(String id) {
        return executableViaSurrogateMap.get(id);
    }

    public List<String> getLinkEndpoints(String elementId) {
        return scope.getLinks().parallelStream()
                .filter(link -> isLinkOfElement(link, elementId))
                .map(link -> getEndpointOfLink(link, elementId))
                .distinct()
                .collect(Collectors.toList());
    }

    private boolean isLinkOfElement(TCollabLink link, String elementId) {
        if (elementId.equals(getCollaboratorOfLink(link).getId()) ||
                elementId.equals(getObjectOfLink(link))) {
            return true;
        }
        return false;
    }

    private String getEndpointOfLink(TCollabLink link, String sourceElementId) {
        if (sourceElementId.equals(getCollaboratorOfLink(link).getId())) {
            return getObjectOfLink(link).getId();
        } else if (sourceElementId.equals(getObjectOfLink(link).getId())) {
            return getCollaboratorOfLink(link).getId();
        }
        return null;
    }

    private TCollaborator getCollaboratorOfLink(TCollabLink link) {
        TAction action = (TAction) link.getCollabActionEndpoint();
        return actionToCollaboratorMap.get(action.getId());
    }

    private TCollabObject getObjectOfLink(TCollabLink link) {
        TAction action = (TAction) link.getObjActionEndpoint();
        return actionToObjectMap.get(action.getId());
    }
}
