package at.ac.tuwien.hadl.domain;

import at.ac.tuwien.hadl.domain.exception.SurrogateException;

public class SurrogateEvent {
    private Surrogate source;
    private SurrogateStatus status;
    private SurrogateException optEx = null;

    public SurrogateException getOptionalEx() {
        return optEx;
    }

    public void setOptionalEx(SurrogateException optEx) {
        this.optEx = optEx;
    }

    public Surrogate getSource() {
        return source;
    }

    public void setSource(Surrogate source) {
        this.source = source;
    }

    public SurrogateStatus getStatus() {
        return status;
    }

    public void setStatus(SurrogateStatus status) {
        this.status = status;
    }

    public SurrogateEvent(Surrogate source, SurrogateStatus status) {
        super();
        this.source = source;
        this.status = status;
    }

    public SurrogateEvent(Surrogate source, SurrogateStatus status,
            SurrogateException optEx) {
        super();
        this.source = source;
        this.status = status;
        this.optEx = optEx;
    }
}
