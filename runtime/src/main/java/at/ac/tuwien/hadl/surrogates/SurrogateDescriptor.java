package at.ac.tuwien.hadl.surrogates;

public class SurrogateDescriptor {
    private String elementId;

    public SurrogateDescriptor(String elementId) {
        super();
        this.elementId = elementId;
    }

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }
}
