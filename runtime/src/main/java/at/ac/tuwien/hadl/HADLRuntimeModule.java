package at.ac.tuwien.hadl;

import at.ac.tuwien.hadl.core.FactoryModule;
import at.ac.tuwien.hadl.marshalling.MarshallingModule;
import at.ac.tuwien.hadl.registry.RegistryModule;

import com.google.inject.AbstractModule;

class HADLRuntimeModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new RegistryModule());
        install(new MarshallingModule());
        install(new FactoryModule());
    }
}