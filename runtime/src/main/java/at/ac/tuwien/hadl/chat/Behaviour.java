package at.ac.tuwien.hadl.chat;

public enum Behaviour {
    SUCCESS, FAIL, TIMEOUT
}
