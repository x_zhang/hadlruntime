package at.ac.tuwien.hadl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.junit.Before;
import org.junit.Test;

import at.ac.tuwien.hadl.chat.Behaviour;
import at.ac.tuwien.hadl.chat.SurrogateAction;
import at.ac.tuwien.hadl.core.SimpleResourceDescriptor;
import at.ac.tuwien.hadl.domain.Scope;
import at.ac.tuwien.hadl.domain.Surrogate;
import at.ac.tuwien.hadl.domain.SurrogateEvent;
import at.ac.tuwien.hadl.domain.SurrogateStatus;
import rx.Observable;

/**
 * Unit test for simple App.
 */
public class ChatTest {
    private CountDownLatch acquireLatch;
    private CountDownLatch releaseLatch;
    private CountDownLatch wireLatch;

    private HADLRuntime runtime;
    private Scope scope;

    private List<Surrogate> surrogates;
    private SimpleResourceDescriptor chatResource;
    private SimpleResourceDescriptor moderatorResource;
    private SimpleResourceDescriptor partyResourceA;
    private SimpleResourceDescriptor partyResourceB;

    @Before
    public void setup() {
        surrogates = new ArrayList<>();

        runtime = new HADLRuntime();
        runtime.init();

        InputStream collabPattern = runtime.getClass().getResourceAsStream(
                "/chat-pattern.xml");

        scope = runtime.createScopeFromResource(collabPattern);
        scope = runtime.allocate(scope);

        chatResource = new SimpleResourceDescriptor(
                "localhost:chat");
        chatResource.addValue("uri", "localhost/chatroom");

        moderatorResource = new SimpleResourceDescriptor(
                "localhost:moderator");
        moderatorResource.addValue("name", "simple moderator");

        partyResourceA = new SimpleResourceDescriptor(
                "localhost:partyA");
        partyResourceA.addValue("name", "alice");

        partyResourceB = new SimpleResourceDescriptor(
                "localhost:partyB");
        partyResourceB.addValue("name", "bob");
    }

    private void handleSurrogateEvent(SurrogateEvent event) {
        System.out.print(event.getSource().getResourceDescriptor()
                .getResourceURI());
        System.out.println(": " + event.getStatus());
        if (event.getOptionalEx() != null) {
            System.out.println(event.getOptionalEx().getMessage());
        }

        if (SurrogateStatus.ACQUIRING_SUCCESS == event.getStatus()) {
            acquireLatch.countDown();
            runtime.wire(event.getSource(), scope).subscribe(
                    this::handleSurrogateEvent);
            surrogates.add(event.getSource());
        }

        if (SurrogateStatus.WIRING_SUCCESS == event.getStatus()) {
            wireLatch.countDown();
        }

        if (SurrogateStatus.UNWIRING_SUCCESS == event.getStatus()) {
            runtime.release(event.getSource(), scope).subscribe(
                    this::handleSurrogateEvent);
        }

        if (SurrogateStatus.RELEASING_SUCCESS == event.getStatus()) {
            releaseLatch.countDown();
        }
    }

    private void waitFinish() {
        try {
            System.out.println("waitig for resources to be acquired");
            acquireLatch.await();
            System.out.println("waiting for resources to be wired");
            wireLatch.await();

            surrogates.stream().forEach(
                    surr -> runtime.unwire(surr, scope).subscribe(
                            this::handleSurrogateEvent));
            System.out.println("waiting for resources to be released");
            releaseLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLifecycleNoException() {
        acquireLatch = new CountDownLatch(4);
        releaseLatch = new CountDownLatch(4);
        wireLatch = new CountDownLatch(4);

        Observable<SurrogateEvent> moderator = runtime.registerFor(scope,
                "moderator", moderatorResource);
        moderator.subscribe(this::handleSurrogateEvent);

        Observable<SurrogateEvent> alice = runtime.registerFor(scope, "party",
                partyResourceA);
        alice.subscribe(this::handleSurrogateEvent);

        Observable<SurrogateEvent> bob = runtime
                .registerFor(scope, "party", partyResourceB);
        bob.subscribe(this::handleSurrogateEvent);

        Observable<SurrogateEvent> chatroom = runtime.registerFor(scope,
                "chatroom", chatResource);
        chatroom.subscribe(this::handleSurrogateEvent);

        waitFinish();
    }

    @Test
    public void testLifecycleAcquireFails() {

        acquireLatch = new CountDownLatch(3);
        releaseLatch = new CountDownLatch(4);
        wireLatch = new CountDownLatch(3);

        Observable<SurrogateEvent> chatroom = runtime.registerFor(scope,
                "chatroom", chatResource);
        chatroom.subscribe(this::handleSurrogateEvent);

        Observable<SurrogateEvent> moderator = runtime.registerFor(scope,
                "moderator", moderatorResource);
        moderator.subscribe(this::handleSurrogateEvent);

        partyResourceA.addValue(SurrogateAction.ACQUIRE.toString(),
                Behaviour.FAIL);
        Observable<SurrogateEvent> alice = runtime.registerFor(scope, "party",
                partyResourceA);
        alice.subscribe(this::handleSurrogateEvent);
        alice.subscribe(event -> {
            if (event.getStatus() == SurrogateStatus.ACQUIRING_FAILED) {
                releaseLatch.countDown();
            }
        });

        Observable<SurrogateEvent> bob = runtime
                .registerFor(scope, "party", partyResourceB);
        bob.subscribe(this::handleSurrogateEvent);

        waitFinish();
    }

    @Test
    public void testConsecutiveRequestsFail() {
        final CountDownLatch successLatch = new CountDownLatch(1);
        final CountDownLatch failedLatch = new CountDownLatch(2);

        Observable<SurrogateEvent> chatroom = runtime.registerFor(scope,
                "chatroom", chatResource);
        chatroom.subscribe(event -> {
            System.out.println(event.getStatus());
            for (int i = 0; i < 3; i++) {
                runtime.wire(event.getSource(), scope)
                        .subscribe(
                                e -> {
                    System.out.println(e.getStatus());
                    if (e.getOptionalEx() != null) {
                        System.out.println(e.getOptionalEx()
                                .getMessage());
                    }
                    if (e.getStatus() == SurrogateStatus.WIRING_SUCCESS) {
                        successLatch.countDown();
                    } else {
                        failedLatch.countDown();
                    }
                });
            }
        });

        try {
            successLatch.await();
            failedLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
