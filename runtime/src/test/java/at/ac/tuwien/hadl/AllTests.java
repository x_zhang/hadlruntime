package at.ac.tuwien.hadl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ChatTest.class })
public class AllTests {
}
